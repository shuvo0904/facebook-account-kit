
<?php
// Initialize variables
$app_id = '<facebook_app_id>';
$secret = '<account_kit_app_secret>';
$version = 'v1.1'; // 'v1.1' for example

$code = $_POST["code"];
$csrf = $_POST["csrf"];


$token_exchange_url = file_get_contents('https://graph.accountkit.com/'.$version.'/access_token?'.'grant_type=authorization_code'.'&code='.$code."&access_token=AA|$app_id|$secret"); 

$access = json_decode($token_exchange_url, true);

if(empty($access) || !isset($access["access_token"])){
    return array("message" => "Unable to varify Phone Number");
};

$appsecret_proof = hash_hmac('sha256', $access["access_token"], $secret);

// Method to send Get request to url
function doCurl($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $data = json_decode(curl_exec($ch), true);
    curl_close($ch);
    return $data;
  }

$me_endpoint_url = 'https://graph.accountkit.com/'.$version.'/me?'.'access_token='.$access["access_token"].'&appsecret_proof='.$appsecret_proof;
$data = doCurl($me_endpoint_url);

if((!isset($data["phone"])) || isset($data["error"])){
    return array("message" => "Unable to varify Phone Number");
}
echo json_encode($data);
?>