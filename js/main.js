$( document ).ready(function() {
    $.ajax({
        type: "GET", 
        dataType: "json", 
        url: "code.json", 
        success: function(response){
            $.each(response,function(i, obj) 
            {
                $('#country_code').append('<option value=' + obj.code + '>' + obj.name +" (" + obj.code + ")" + '</option>');
            });

        }
     });
});



 // initialize Account Kit with CSRF protection
  AccountKit_OnInteractive = function(){
    AccountKit.init(
      {
        appId:"{{FACEBOOK_APP_ID}}", //FaceBook APP Id
        state:"{{csrf}}",  //CSRF protection: The {{csrf}} placeholder above should be replaced with a non-guessable value that should originate in the app's server and be passed to the login flow. It is returned back to the app client unchanged, and the app client can pass it back to server to verify the match with the original value.
        version:"v1.1",
        fbAppEventsEnabled:true
      }
    );
  };

  // login callback
  function loginCallback(response) {
    if (response.status === "PARTIALLY_AUTHENTICATED") {
      var code = response.code;
      var csrf = response.state;
      // Send code to server to exchange for access token
      var data = {
          code : code,
          csrf: csrf
      };
      $.ajax({
        type: "POST", 
        dataType: "json", 
        url: "varification.php", 
        data: data,
        success: function(response){
            console.log("response", response);
            $("#details").css("display","block");
            $("#id").text(response.id);
            $("#country_prefix").text(response.phone.country_prefix);
            $("#national_number").text(response.phone.national_number);
            $("#number").text(response.phone.number);
        }
     });

    }
    else if (response.status === "NOT_AUTHENTICATED") {
        $(".fbmessage").append("<p> NOT AUTHENTICATED status received from facebook, something went wrong. </p>");
    }
    else if (response.status === "BAD_PARAMS") {
        $(".fbmessage").append("<p> NOT AUTHENTICATED status received from facebook, something went wrong. </p>");
    }
  }

  // phone form submission handler
  function smsLogin() {
    var countryCode = document.getElementById("country_code").value;
    var phoneNumber = document.getElementById("phone_number").value;

    $(".fbmessage").append("<p>Sms Login Function Call</p>");

    AccountKit.login(
      'PHONE', 
      {countryCode: countryCode, phoneNumber: phoneNumber}, // will use default values if not specified
      loginCallback
    );
  }


  // email form submission handler
  function emailLogin() {
    var emailAddress = document.getElementById("email").value;

    $(".fbmessage").append("<p> Email Login Function Call </p>");


    AccountKit.login(
      'EMAIL',
      {emailAddress: emailAddress},
      loginCallback
    );
  }
