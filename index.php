<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Facebook Account Kit</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://sdk.accountkit.com/en_US/sdk.js"></script>
    <script src="js/main.js"></script>
</head>
<body>
    <div class="container mt25">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="exampleInputEmail1">Country Code</label>
                    <select class="form-control" id="country_code">
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="exampleInputEmail1">Phone Number</label>
                    <input type="number" class="form-control"  placeholder="Phone Number" id="phone_number">
                </div>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-success btn-block mt25" onclick="smsLogin()">Login Via SMS</button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email Address</label>
                    <input type="email" class="form-control"  placeholder="Email Address" id="email">
                </div>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-success btn-block mt25" onclick="emailLogin()">Login Via Email</button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="fbmessage"></div>
            </div>
        </div>
        <div class="row" style="display:none;" id="details">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <tr>
                          <th>Id</th>  
                          <th>Country Prefix</th>  
                          <th>National Number</th>  
                          <th>Number</th>  
                    </tr>
                    <tr>
                          <th><span id="id"></span></th>  
                          <th><span id="country_prefix"></span></th>  
                          <th><span id="national_number"></span></th>  
                          <th><span id="number"></span></th>  
                    </tr>
                </table>
            </div>
        </div>
    </div>
</body>
</html>